#pragma once

#include "ofMain.h"
#include "ofUtils.h"
#include "serial/serial.h"

using WoodSerial = serial::Serial;

namespace ofxWoodSerial {
//--------------------------------------------------------------
bool findPath(const string & physicalPort,
              string & path);
bool findSerial(const string & serialNumber,
                string & path);
bool readLine(WoodSerial & serial,
              string & line);
}
