#include "ofxWoodSerial.h"

bool ofxWoodSerial::findPath(const string & physicalPort,
                             string & path){
    ofDirectory dir("/dev");
    dir.listDir("/dev/");
    for(int i = 0; i < dir.size(); i++){
        const string & p = dir.getPath(i);
        // also look for ACM, if you look for arduino
        if(ofIsStringInString(p, "USB") || ofIsStringInString(p, "ACM")){
            // use udevadm (Linux only) to get info about the path
            // and check if it mentions the port we are looking for
            string cmd = "udevadm info --query=all --attribute-walk --name=" + p + " | grep \"" + physicalPort + "\"";
            // it returns a value, only if it found the port
            if(ofSystem(cmd.c_str()) != ""){
                path = p;
                return true;
            }
        }
    }
    return false;
}

bool ofxWoodSerial::findSerial(const string & serialNumber,
                               string & path){
    ofDirectory dir("/dev");
    dir.listDir("/dev/");
    for(int i = 0; i < dir.size(); i++){
        const string & p = dir.getPath(i);
        // also look for ACM, if you look for arduino
        if(ofIsStringInString(p, "USB") || ofIsStringInString(p, "ACM")){
            // get string to search for
            string search = "ATTRS{serial}==\\\"" + serialNumber + "\\\"";
            // use udevadm (Linux only) to get info about the path
            // and check if it mentions the port we are looking for
            string cmd = "udevadm info --query=all --attribute-walk --name=" + p + " | grep \"" + search + "\"";
            // it returns a value, only if it found the port
            if(ofSystem(cmd.c_str()) != ""){
                path = p;
                return true;
            }
        }
    }
    return false;
}

bool readMessage(serial::Serial & ws,
                 int start_marker,
                 int stop_marker){
    return false;
}
