#pragma once

#include "ofMain.h"
#include "ofxNetwork.h"
#include "ofxWoodSerial.h"

class ofApp : public ofBaseApp {

    public:
        void setup();
        void update();
        void draw();
        void enumeratePorts();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y);
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

        ofxUDPManager udp;
        shared_ptr <ofxWoodSerial> serial;
        uint64_t baud = 9600;
};
