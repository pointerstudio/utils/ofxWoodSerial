#!/bin/bash

# ------------------------------------------------------------ HELLO
cat <<EOF
STUDIO       _       _
 _ __   ___ (_)_ __ | |_ ___ _ ____/\__
| '_ \ / _ \| | '_ \| __/ _ \ '__\\    /
| |_) | (_) | | | | | ||  __/ |  /_  _\\
| .__/ \___/|_|_| |_|\__\___|_|    \/
|_|
      presents: $0

EOF

# ------------------------------------------------------------ HELPER FUNCTIONS
decision() {
    # get default
    if [ "$2" = 0 ]; then 
        default="yes"
    else
        default="no"
    fi

    # should we go autopilot
    if [ "${REMEMBORY_AUTOPILOT}" = 1 ];
    then
        echo "$1?"
        return $2
    fi

    echo ""
    echo "***********************************"
    echo "* $0 NEEDS A DECISION"
    echo ""
    echo "$1?"
    read -r -p "* Do you want? (y)es / (n)o / (d)efault - $default " input

    case $input in
        [yY][eE][sS]|[yY])
            return 0
            ;;
        [nN][oO]|[nN])
            return 1
            ;;
        [dD])
            return $2
            ;;
        *)
            return 1
            ;;
    esac
}

# ------------------------------------------------------------ VARIABLES
# dynamic
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# defaults
if [ -z "$REMEMBORY_AUTOPILOT" ];
then
    REMEMBORY_AUTOPILOT=0
fi
PROBABLY_YES=0
PROBABLY_NOT=1

if decision "link library in ~/.profile?" $PROBABLY_YES;
then
    cat <<"EOF" >> ~/.profile

if [ -z "$LD_LIBRARY_PATH" ]
then
    export LD_LIBRARY_PATH=$PG_OF_PATH/addons/ofxWoodSerial/libs/serial/lib/linux64/
else
    export LD_LIBRARY_PATH=$PG_OF_PATH/addons/ofxWoodSerial/libs/serial/lib/linux64/:$LD_LIBRARY_PATH
fi
EOF
fi
