#include "ofApp.h"
#include "serial/serial.h"

//--------------------------------------------------------------
void ofApp::setup(){

    enumeratePorts();
    string lightPort;
    ofSetFrameRate(15);

    if(!findPhysicalPort("7-3", lightPort)){
        ofExit(1);
    }
    ofLogNotice() << "found port! " << lightPort;

    serial = make_shared <serial::Serial>(lightPort.c_str(), baud, serial::Timeout::simpleTimeout(1000));

    cout << "Is the serial port open?";
    if(serial->isOpen()){
        cout << " Yes." << endl;
    }else{
        cout << " No." << endl;
    }

    string all = "0\r";
    string on = "0\r";
    string off = "127\r";

    string msg_sync = "155\r";

    bool synced = false;
    for(int i = 1; i <= 6; i++){
        values.push_back(127);
    }
    for(int i = 1; i <= 6 && !synced; i++){
        serial->write(all);
        usleep(20 * 1000);
        serial->write(msg_sync);
        usleep(20 * 1000);
        string result = serial->read(256);
        if(result.substr(result.length() - 10) == "range\r\nCH:"){
            synced = true;
            ofLogNotice() << "we are synced with (" << i << "): " << result;
        }
    }

    string result = serial->read(256);

    cout << "Iteration: , Bytes written: ";
    cout << result.length() << ", String read: " << result << endl;

}

//--------------------------------------------------------------
void ofApp::update(){
    for(int i = 0; i < 6; i++){
        int intermediate = int((0.5 + 0.5 * sin(float(i * 1.0 + ofGetElapsedTimeMillis() * 0.001))) * 127.0f);
        int value = ofMap(intermediate, 0, 127, 60, 122);
        int c = i + 1;
        write(serial, c, value);
        cout << i << " : " << value << endl;
        values[i] = value;
    }
}

void ofApp::write(shared_ptr <serial::Serial> & s,
                  const int & index,
                  const int & value){
    serial->write((ofToString(index) + "\r"));
    usleep(10 * 1000);
    serial->write((ofToString(value) + "\r"));
    usleep(10 * 1000);
}

//--------------------------------------------------------------
void ofApp::draw(){
    for(int i = 0; i < values.size(); i++){
        ofRectangle r;
        float w = ofGetWidth() / values.size();
        float h = ofGetHeight();
        float x = i * w;
        float y = 0;
        int rgb = int(ofMap(values[i], 127, 60, 0, 255, true));
        ofSetColor(ofColor(rgb));
        ofDrawRectangle(x, y, w, h);
    }
}

void ofApp::exit(){
    cout << "now off" << endl;
    for(int i = 1; i <= 6; i++){
        string index = ofToString(i) + "\r";
        serial->write(index);
        usleep(10 * 1000);
        serial->write("127\r");
        usleep(10 * 1000);
    }
    serial->close();
}

void ofApp::enumeratePorts(){
    vector <serial::PortInfo> devices_found = serial::list_ports();

    vector <serial::PortInfo>::iterator iter = devices_found.begin();

    while(iter != devices_found.end()){
        serial::PortInfo device = *iter++;

        printf("(%s, %s, %s, %s)\n", device.port.c_str(), device.description.c_str(),
               device.hardware_id.c_str(), device.port.c_str());
    }
}
//--------------------------------------------------------------
bool ofApp::findPhysicalPort(const string & physicalPort, string & path){
    ofDirectory dir("/dev");
    dir.listDir("/dev/");
    ofLogNotice("findPhysicalPort") << "looking for " << physicalPort;
    for(int i = 0; i < dir.size(); i++){
        const string & p = dir.getPath(i);
        // also look for ACM, if you look for arduino
        if(ofIsStringInString(p, "USB")){
            // use udevadm (Linux only) to get info about the path
            // and check if it mentions the port we are looking for
            string cmd = "udevadm info --query=all --attribute-walk --name=" + p + " | grep \"" + physicalPort + "\"";
            // it returns a value, only if it found the port
            if(ofSystem(cmd.c_str()) != ""){
                path = p;
                return true;
            }
        }
    }
    return false;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
