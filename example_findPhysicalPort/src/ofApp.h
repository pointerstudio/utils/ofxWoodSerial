#pragma once

#include "ofMain.h"
#include "ofxNetwork.h"
#include "serial/serial.h"

class ofApp : public ofBaseApp {

    public:
        void setup();
        void update();
        void draw();
        void enumeratePorts();
        bool findPhysicalPort(const string & physicalPort,
                              string & path);
        void write(shared_ptr <serial::Serial> & s,
                   const int & index,
                   const int & value);
        void exit();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y);
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

        ofxUDPManager udp;
        shared_ptr <serial::Serial> serial;
        uint64_t baud = 9600;
        vector <int> values;
};
